# Counting Game

## A simple counting game made in nodejs.

This is a simple counting game made in nodejs that asks the user a simple math question.
The game can save scores and later read them. 

## Usage

You start the game using
```
$ node index.js --operation *operation*
```
Supported operators are
* addition
* subtraction
* multiplication
* division **(beta)**

### Exapmple:

```
$ node index.js --operation multiplication
```

## Installation
```
$ git clone https://gitlab.com/blyatinator/countingGame.git
$ cd countingGame
$ npm install
```

By Hugo Blomdahl