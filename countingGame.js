//Required packages
const prompts = require('prompts');
const flags = require('flags');
const fs = require('fs-extra');
const yaml = require('js-yaml');
const Table = require('easy-table');

//Define and parse flags
flags.defineString('operation', 'unknown', 'The operation to be run');
flags.parse();
 //Declare variables
let operation = flags.get('operation').toLowerCase();
let operator;
let number = [];
let answer = 0;
let questions = 10;
let table = [];
let question;
let filenumber;

//A function that returns a random number that is not 0
function numberGenerator(t){
  let x;
  
  for(k=0; k!=1;){
  x = Math.floor(Math.random()*t);
  if(x != 0){
  return(x);
  k = 1;
  }
  }
}

//Check what flag it is
if (operation === "unknown") console.log('To run this program you need to set a paramater. Try: \"node index.js --operation=addition\"');

else if (operation === "addition") operator = " + ";

else if (operation === "subtraction") operator = " - ";

else if (operation === "division") operator = " / ";

else if (operation === "multiplication") operator = " * ";

//Print error message and dont set operator so the game can't start
else console.log('Unknown flag');

//If the operator is not undefined the game will start
if (operator != undefined){
  //A lot of the coming code is taken from the npm packet examples, especially from https://www.npmjs.com/package/prompts
      (async () => {
        //Loop as many times as there is questions
        for(i=0; i<questions; i++){
          //Generate two numbers
          for(j=0; j<2; j++){
            number.push(numberGenerator(10))
            }
            //Ask user for answer to simple math "problem"
        const response = await prompts({
          type: 'number',
          name: 'userAnswer',
          float: 'true',
          message: 'What is ' + number[i] + operator + number[i+1] + "?"
        });
        //If the answer is correct, add it to a list and add to the number of correct answers
        if(response.userAnswer === eval(number[i] + operator + number[i+1])){
        table.push({ question: number[i] + operator + number[i+1], answer: response.userAnswer, correct: true });
        answer++; }
        //Otherwise just add to the table
        else
        table.push({ question: number[i] + operator + number[i+1], answer: response.userAnswer, correct: false });
      }
      //Print the nice looking table
      console.log(Table.print(table, {
        question: {name: 'Question'},
        answer: {name: 'Answer'},
        correct: {name: 'Correct answer?'}
      }));
      
      table.push({ ncorrect: answer });
      
      console.log("You got " + answer + "/" + questions + " correct");

      //Ask the user if they want to save and if they do ask what name they have
      const response = await prompts({
        type: 'confirm',
        name: 'save',
        message: 'Do you want to save your result?'
      })

      if(response.save === true){
      const response = await prompts({
        type: 'text',
        name: 'user',
        message: 'What is your name?'
      })

      //Make folder for user, this does not return an error if the folder already exists
      let username = response.user.toLowerCase();
      fs.mkdir("saves/" + username, (err) => {
    });
      //Check how many saves player already has
      fs.readdir('saves/' + username + '/', (err, files) => {
        filenumber = files.length + 1;
      //Save the file as yaml
      fs.writeFile("saves/" + username + '/' + filenumber +'.yml', yaml.safeDump(table),  (err) => {
        if (err)
        console.log(err);
        });
      });
    }
  })();
}

//by Hugo Blomdahl